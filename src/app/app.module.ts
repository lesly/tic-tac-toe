import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { NgModule } from '@angular/core';
import { Observable } from 'rxjs';
import { RouterModule, Routes } from '@angular/router';
import { FlexLayoutModule } from "@angular/flex-layout";

import {
  MatSnackBarModule,
  MatToolbarModule,
  MatTooltipModule,
  MatIconModule,
  MatCardModule,
  MatGridListModule,
  MatListModule,
  MatInputModule,
  MatDividerModule,
  MatButtonModule,
} from '@angular/material';

import { AppComponent } from './app.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { CardScoresComponent } from './components/card-scores/card-scores.component';
import { ContainerComponent } from './components/container/container.component';
import { CardWelcomeComponent } from './components/card-welcome/card-welcome.component';
import { CardGameComponent } from './components/card-game/card-game.component';
import { GamePlayComponent } from './components/game-play/game-play.component';
import { GameWinnerComponent } from './components/game-winner/game-winner.component';

const appRoutes: Routes = [
  { path: '', component: ContainerComponent },
  { path: 'scores', component: CardScoresComponent },
  { path: '**', component: ContainerComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    CardGameComponent,
    CardWelcomeComponent,
    ContainerComponent,
    GamePlayComponent,
    GameWinnerComponent,
    CardScoresComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    LayoutModule,
    FlexLayoutModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatGridListModule,
    MatListModule,
    MatInputModule,
    MatDividerModule,
    MatButtonModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
