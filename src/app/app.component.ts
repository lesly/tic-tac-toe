import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  play: boolean = false;
  score: boolean = false;

  constructor(
    public _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    let snackBarRef = this._snackBar.open('En attente de joueurs...');
    localStorage.setItem('winner', '');
  }

  getScores() {
    this.play = true;
    this.score = true;
  }

  playApp() {
    let snackBarRef = this._snackBar.open('En attente de joueurs...');
    this.play = true;
    this.score = false;
  }

  quitApp() {
    let snackBarRef = this._snackBar.open('En attente de joueurs...');
    this.play = false;
    this.score = false;
    localStorage.setItem('player1', '');
    localStorage.setItem('player2', '');
  }

}
