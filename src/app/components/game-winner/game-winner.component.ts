import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'cg-game-winner',
  templateUrl: './game-winner.component.html',
  styleUrls: ['./game-winner.component.css']
})
export class GameWinnerComponent implements OnInit {

  winner: string;

  constructor() { }

  @Output() onRetryGame: EventEmitter<boolean> = new EventEmitter<boolean>();

  public retry(): void {
    localStorage.setItem('winner', 'inconnu');
    this.onRetryGame.emit(true);
  }

  ngOnInit() {
    this.winner = localStorage.getItem('winner');
  }

}
