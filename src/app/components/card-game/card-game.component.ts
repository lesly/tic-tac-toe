import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cg-card-game',
  templateUrl: './card-game.component.html',
  styleUrls: ['./card-game.component.css']
})
export class CardGameComponent implements OnInit {

  end: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  endGame() {
    this.end = true;
  }

  retryGame() {
    this.end = false;
  }

}
