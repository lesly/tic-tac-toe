import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cg-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css']
})
export class ContainerComponent implements OnInit {

  game: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  playGame() {
    this.game = true;
  }

}
