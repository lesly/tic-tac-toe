import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'cg-game-play',
  templateUrl: './game-play.component.html',
  styleUrls: ['./game-play.component.css']
})
export class GamePlayComponent implements OnInit {

  grid;
  score;
  scores;
  scoresPush;
  player: number = 1;
  winner: number = null;
  snackPlayer: string;
  end: boolean = false;

  constructor(
    public _snackBar: MatSnackBar
  ) { }

  @Output() onEndGame: EventEmitter<boolean> = new EventEmitter<boolean>();

  public win(): void {
    this.onEndGame.emit(true);
  }

  ngOnInit() {
    this.reloadGame();
  }

  reloadGame() {
    this.player = 1;
    this.winner = null;
    this.end = false;
    this.getSnackBarPlay();
    this.getGrid();
  }

  getGrid() {
    this.grid = [
      {id: 0, player: null, played: false},
      {id: 1, player: null, played: false},
      {id: 2, player: null, played: false},
      {id: 3, player: null, played: false},
      {id: 4, player: null, played: false},
      {id: 5, player: null, played: false},
      {id: 6, player: null, played: false},
      {id: 7, player: null, played: false},
      {id: 8, player: null, played: false},
    ];
  }

  getPlayer(): string {
    if (this.player===1) {
      return localStorage.getItem('player1');
    } else {
      return localStorage.getItem('player2');
    }
  }

  setWinner() {
    if (this.winner===1) {
      localStorage.setItem('winner', localStorage.getItem('player1'));
    } else {
      localStorage.setItem('winner', localStorage.getItem('player2'));
    }
    this.scoresPush = JSON.parse(localStorage.getItem('scores'));
    this.scores = [];
    this.score = {winner: localStorage.getItem('winner'), player1: localStorage.getItem('player1'), player2: localStorage.getItem('player2'), date: new Date()};
    this.scores.push(this.score);
    if (this.scoresPush!=null) {
      this.scoresPush.forEach(data => {
        this.scores.push(data);
      });
    }
    localStorage.setItem('scores', JSON.stringify(this.scores));
    this.win();
  }

  getSnackBarPlay() {
    let snackBarRef = this._snackBar.open('C\'est au tour de ' + this.getPlayer() + ' de jouer.');
    if (this.end===true) {
      snackBarRef.dismiss();
    }
  }

  play(item) {
    if (this.end===false) {
      this.grid[item].player = this.player;
      this.grid[item].played = true;
      this.detectEndGame();
      if (this.player===1) {
        this.player = 2;
      } else {
        this.player = 1;
      }
      this.getSnackBarPlay();
    }
  }

  detectEndGame() {
    // Possiblité 3,6,9
    if (this.grid[0].played&&this.grid[1].played&&this.grid[2].played&&this.grid[3].played&&this.grid[4].played&&this.grid[5].played&&this.grid[6].played&&this.grid[7].played&&this.grid[8].played) {
      this.end = true;
      localStorage.setItem('winner', 'inconnu');
      this.win();
    }
    // Possiblité 1,2,3
    if (this.grid[0].player==this.player&&this.grid[1].player==this.player&&this.grid[2].player==this.player) {
      this.end = true;
      this.winner = this.player;
      this.setWinner();
    }
    // Possiblité 4,5,6
    if (this.grid[3].player==this.player&&this.grid[4].player==this.player&&this.grid[5].player==this.player) {
      this.end = true;
      this.winner = this.player;
      this.setWinner();
    }
    // Possiblité 7,8,9
    if (this.grid[6].player==this.player&&this.grid[7].player==this.player&&this.grid[8].player==this.player) {
      this.end = true;
      this.winner = this.player;
      this.setWinner();
    }
    // Possiblité 1,5,9
    if (this.grid[0].player==this.player&&this.grid[4].player==this.player&&this.grid[8].player==this.player) {
      this.end = true;
      this.winner = this.player;
      this.setWinner();
    }
    // Possiblité 3,5,7
    if (this.grid[2].player==this.player&&this.grid[4].player==this.player&&this.grid[6].player==this.player) {
      this.end = true;
      this.winner = this.player;
      this.setWinner();
    }
    // Possiblité 1,3,7
    if (this.grid[0].player==this.player&&this.grid[3].player==this.player&&this.grid[6].player==this.player) {
      this.end = true;
      this.winner = this.player;
      this.setWinner();
    }
    // Possiblité 2,5,8
    if (this.grid[1].player==this.player&&this.grid[4].player==this.player&&this.grid[7].player==this.player) {
      this.end = true;
      this.winner = this.player;
      this.setWinner();
    }
    // Possiblité 3,6,9
    if (this.grid[2].player==this.player&&this.grid[5].player==this.player&&this.grid[8].player==this.player) {
      this.end = true;
      this.winner = this.player;
      this.setWinner();
    }
  }

}
