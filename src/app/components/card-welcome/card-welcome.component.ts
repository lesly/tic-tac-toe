import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'cg-card-welcome',
  templateUrl: './card-welcome.component.html',
  styleUrls: ['./card-welcome.component.css']
})
export class CardWelcomeComponent implements OnInit {

  error: string = null;

  constructor() { }

  ngOnInit() {
    if (localStorage.getItem('player1')!=''&&localStorage.getItem('player2')!='') {
      this.onPlay.emit(true);
    }
  }

  @Output() onPlay: EventEmitter<boolean> = new EventEmitter<boolean>();

  play(player1: string, player2: string): void {
    if (player1==''&&player2=='') {
      this.error = 'Merci d\'indiquer le nom des deux joueurs.';
    } else if (player1=='') {
      this.error = 'Merci d\'indiquer le nom du joueur 1.';
    } else if (player2=='') {
      this.error = 'Merci d\'indiquer le nom du joueur 2.';
    } else {
      localStorage.setItem('player1', player1);
      localStorage.setItem('player2', player2);
      this.onPlay.emit(true);
    }
  }

}
