import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'cg-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  constructor() { }

  @Input('play') play;
  @Input('score') score;
  @Output() onBegin: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() onExit: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() onScore: EventEmitter<boolean> = new EventEmitter<boolean>();

  public begin(): void {
    this.play = true;
    this.score = false;
    this.onBegin.emit(true);
  }

  public scores(): void {
    this.play = false;
    this.score = true;
    this.onScore.emit(true);
  }

  public quit(): void {
    this.play = false;
    this.score = false;
    this.onExit.emit(true);
  }

  ngOnInit() {
  }

}
