import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'cg-card-scores',
  templateUrl: './card-scores.component.html',
  styleUrls: ['./card-scores.component.css']
})
export class CardScoresComponent implements OnInit {

  scores;
  display: number = 5;

  constructor(
    public _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    let snackBarRef = this._snackBar.open('Chargement des scores...');
    this.scores = JSON.parse(localStorage.getItem('scores'));
    snackBarRef.dismiss();
  }

}
