import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardScoresComponent } from './card-scores.component';

describe('CardScoresComponent', () => {
  let component: CardScoresComponent;
  let fixture: ComponentFixture<CardScoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardScoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardScoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
